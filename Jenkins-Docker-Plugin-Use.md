##Jenkins Docker相关的Plugin使用##
###Jenkins与Docker相关的Plugin###
在[Jenkins Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Plugins)页面搜索与Docker相关的插件，有如下几个：

- CloudBees Docker Build and Publish plugin — This plugin provides the ability to build projects with a Dockerfile, and publish the resultant tagged image (repo) to the docker registry.
-  Docker build step plugin — This plugin allows to add various Docker commands into you job as a build step.
-  Docker Plugin — This plugin allows slaves to be dynamically provisioned using Docker.
-  Kubernetes Plugin — This plugin allows slaves to be dynamically provisioned on multiple Docker hosts using Kubernetes.
-  Docker Commons Plugin — APIs for using Docker from other plugins.

其中，它们间的关系如下：

-  Docker commons Plugin为其他与Docker相关的插件提供了APIs
-  CloudBees Docker Build and Publish plugin和Docker build step plugin都依赖了Docker commons Plugin
-  Kubernetes Plugin依赖了Docker Plugin

下面主要介绍下Docker build step plugin和CloudBees Docker Build and Publish plugin的使用。

###Docker build step plugin使用###

####设置Docker URL####
系统管理→系统设置→Docker Builder，设置Docker URL并测试连接。

![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-builder-config.png)

####在Jenkins JOB的构建区域，增加构建步骤→Execute Docker container####
有一系列Docker Command可选择

![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-01.png)
![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-02.png)

####以重启一个容器为例####
选择Restart containers命令，并填写Container ID(s)：

![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-03.png)

Jenkins JOB创建成功后，点击构建，日志输出如下：

![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-04.png)

####以Push镜像到Docker registry为例####
选择Push images命令，并填写相关信息：

![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-05.png)

Jenkins JOB创建成功后，点击构建，日志输出如下：


![docker-build-step](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-step-06.png)


###Docker Build Publish Plugin使用###

####设置源码地址，这里使用的是GIT@OSC####
该项目是个Docker化的项目，Dockerfile在根目录下

![docker-build-publish](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-publish-git.png)

####在Jenkins JOB的构建区域，增加构建步骤→Docker Build and Publish####
![docker-build-publish](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-publish-02.png)

此外，Docker Build Publish Plugin还要一些高级选项

![docker-build-publish](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-publish-03.png)

####Jenkins JOB创建成功后，点击构建，日志输出如下####
![docker-build-publish](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/jenkins-docker-plugin-pic/docker-build-publish-04.png)


###参考###
- [Docker+build+step+plugin](https://wiki.jenkins-ci.org/display/JENKINS/Docker+build+step+plugin)
- [Docker+Build+and+Publish+plugin](https://wiki.jenkins-ci.org/display/JENKINS/CloudBees+Docker+Build+and+Publish+plugin)
