#使用Docker搭建GitLab实践#
##SVN与GIT，二者皆须会##
当前版本控制系统（Version Control System，VCS）有集中化版本版本控制系统（Centralized Version Control System，简称 CVCS）和分布式版本控制系统（Distributed Version Control System，简称 DVCS）。
<br/>
集中化版本控制系统的代表是SVN，分布式版本控制系统的代表是GIT。

熟悉SVN已两年，玩过SVNKIT、svn cli、pysvn，当然也搭建过SVN服务端。
<br/>
平常偶尔也通过GitHub或Git@OSC使用GIT，web界面所提供的功能强大，简单易用。
<br/>
而SVN在业界目前没发现有开源的成熟的类似GitHub这样的解决方案；
<br/>
GitLab号称是GitHub的开源实现，它拥有与Github类似的功能，其中Git@OSC就是基于 GitLab 项目搭建。
<br/>
也一直有想法自己搭建GitLab，用来体验、使用、熟悉它。
<br/>
而查阅过多篇关于GitLab的搭建的官方文档或博客，深深的感觉搭建过程过于繁琐，非一日之功，于是搭建GitLab计划就被搁浅了。
<br/>
当然，也有第三方提供一键安装Gitlab的解决方案（如Bitnami GitLab Installers），但不是很感冒，并没有做尝试。
##Docker，让GitLab安装部署更简单##
而今Docker流行，它使得软件安装部署变得更简单，通过Docker镜像与容器就可以快速搞定这些繁琐的、重复的安装部署过程，并且镜像可移植。
<br/>
之前曾用Docker部署过Jenkins和ReviewBoard，切身体会到了Docker的带来的便利。
<br/>
有了之前对Docker的了解与实践，于是开始了使用Docker搭建GitLab的实践。
<br/>
GitLab的Docker镜像已经有大牛做好，并且一直在随着GitLab这个版本帝的升级而进行维护，详见其[GitHub仓库](https://github.com/sameersbn/docker-gitlab)，并且文档也很健全。

##使用Docker搭建GitLab实践##
###使用docker-compose快速启动Gitlab###
    
	wget https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml
	docker-compose up
###三步走运行GitLab容器###
####1、运行一个PostgreSQL容器####

	docker run --name gitlab-postgresql -d \
    --env 'DB_NAME=gitlabhq_production' \
    --env 'DB_USER=gitlab' --env 'DB_PASS=password' \
    --volume /srv/docker/gitlab/postgresql:/var/lib/postgresql \
    sameersbn/postgresql:9.4-2

####2、运行一个Redis容器####

	docker run --name gitlab-redis -d \
    --volume /srv/docker/gitlab/redis:/var/lib/redis \
    sameersbn/redis:latest

####3、运行GitLab容器####

	docker run --name gitlab -d \
    --link gitlab-postgresql:postgresql --link gitlab-redis:redisio \
    --publish 10022:22 --publish 10080:80 \
    --env 'GITLAB_PORT=10080' --env 'GITLAB_SSH_PORT=10022' \
    --volume /srv/docker/gitlab/gitlab:/home/git/data \
	sameersbn/gitlab:7.13.1

**注意：GitLab应用的启动需要几分钟。**

###通过csphere查看gitlab相关容器###
![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-01.png)

###访问GitLab###
访问地址：http://localhost:10080，使用默认的用户名和密码登录。
<br/>
默认的用户名：**root**
<br/>
默认的密码：**5iveL!fe**
<br/>

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-02.png)

GitLab前不久启用了全新logo，界面布局也有变化：

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-03.png)

##GitLab更多配置##
通过上面的步骤已经快速启动了GitLab容器，可以用来测试使用，但是要在生产环境使用GitLab还需要进行一系列配置。
<br/>
GitLab的一系列配置信息（如：GitLab_HOST、Mail、LDAP等）目前还无法从web界面进行配置。
<br/>
而docker-gitlab为这提供了以环境变量的形式提供了[一系列可配置的参数](https://github.com/sameersbn/docker-gitlab#available-configuration-parameters)。
<br/>
这些环境变量需要在GitLab镜像启动的时候指定。
<br/>
如果当前GitLab容器已启动，可以停止、删除容器，然后基于GitLab镜像指定环境变量再创建、启动新容器即可。

###GITLAB_HOST配置###
GITLAB_HOST: The hostname of the GitLab server. Defaults to localhost
<br/>
这个值会被Gitlab用来生成repo的链接，所以必须要设置。否则，在创建的repo中，会发现所有的repo链接都是以localhost为hostname。

###MAIL配置###
mail可以用来在用户注册的时候给用户发送邮箱认证链接相关信息。
<br/>
默认的mail配置使用的是gmail，需要一个用户名和密码来登录到gmail服务器。
<br/>
当然，也可以通过指定一系列SMTP相关的环境变量来使用其他邮箱（如QQ邮箱、网易邮箱）作为邮件服务器。

###时区配置###
GitLab默认的时区是UTC，北京时区为UTC+8区。
<br/>
可以通过指定环境变量GITLAB_TIMEZONE=Beijing来更改时区。

包括但不限于上面的配置，GitLab还可以定制更多配置信息，如LDAP、SSL、OmniAuth Integration等等，详情请参考官方说明文档。

##Docker-GitLab搭建遇到的问题##
###磁盘空间不足###
PostgreSQL容器在启动之后迅速关闭，再次启动亦是如此，且并没有给出错误提示信息。
<br/>
通过使用docker logs gitlab-postgresql查看容器日志时获得：No space left on device，在主机上清理磁盘空间后解决了该问题。
###页面提供的访问gitlab的默认用户名和密码不准确（目前已修正）###
页面提供的默认密码为password，登录失败，后来使用5iveL!fe这个登录成功。
<br/>
查看docker-gitlab的提交日志，关于默认密码，有如下行：

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-04.png)

同时，与之相关的还有[issue 389](https://github.com/sameersbn/docker-gitlab/issues/389)：

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-05.png)
##为Docker-GitLab做贡献##
在阅读docker-gitlab的README时，发现文档有一处使用的fig（docker-compose的前身），
<br/>
于是fork了docker-gitlab的源码，并提交了一个pull request将fig修改为docker-compose，
<br/>
目前该PR已经被Merged，很荣幸自己也成为docker-gitlab的一个contributor。

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-06.png)

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-07.png)

![docker-gitlab-use](https://git.oschina.net/donhui/md-doc-with-pic/raw/master/docker-gitlab-use-pic/docker-gitlab-08.png)

##后续Docker-GitLab研究计划##
- 研究docker-gitlab的其他配置，如LDAP、SSL、OmniAuth Integration等
- 研究docker-gitlab的安装配置文件（docker-gitlab/assets）
- 研究docker-gitlab的Dockerfile
- 研究docker-gitlab的entrypoint.sh（该shell脚本近千行）

##参考##
- [docker-gitlab官方说明文档](https://github.com/sameersbn/docker-gitlab)
- [docker-gitlab部署(from segmentfault)](http://segmentfault.com/a/1190000002421271)